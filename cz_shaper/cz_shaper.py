import os
import re

from commitizen import defaults
from commitizen.cz.base import BaseCommitizen
from commitizen.cz.utils import multiple_line_breaker, required_validator
from commitizen.defaults import Questions

def parse_scope(text):
    if not text:
        return ""

    scope = text.strip().split()
    if len(scope) == 1:
        return scope[0]

    return "-".join(scope)


def parse_subject(text):
    if isinstance(text, str):
        text = text.strip(".").strip()

    return required_validator(text, msg="Описание необходимо для создания изменения.")


class ShaperCz(BaseCommitizen):
    bump_pattern = defaults.bump_pattern
    bump_map = defaults.bump_map
    bump_map_major_version_zero = defaults.bump_map_major_version_zero
    commit_parser = defaults.commit_parser
    change_type_map = {
        "feat": "Features",
        "fix": "Bug Fixes",
        "refactor": "Refactor",
        "perf": "Performance",
    }
    changelog_pattern = defaults.bump_pattern

    def questions(self) -> Questions:
        questions: Questions = [
            {
                "type": "list",
                "name": "prefix",
                "message": "Выберите тип изменения, которое вы фиксируете",
                "choices": [
                    {
                        "value": "fix",
                        "name": "🔧 исправление: Исправление ошибок.",
                        "key": "x",
                    },
                    {
                        "value": "feat",
                        "name": "✨ функциональность: Новые возможности.",
                        "key": "f",
                    },
                    {
                        "value": "docs",
                        "name": "📚 документация: Изменения только в документации.",
                        "key": "d",
                    },
                    {
                        "value": "style",
                        "name": "💅 стиль: Изменения, которые не влияют на смысл кода (пробелы, форматирование, отсутствующие точки с запятой и т. д.).",
                        "key": "s",
                    },
                    {
                        "value": "refactor",
                        "name": "🛠️ переработка: Изменение кода, которое не исправляет ошибку и не добавляет новую функциональность.",
                        "key": "r",
                    },
                    {
                        "value": "perf",
                        "name": "⚡ производительность: Изменение кода, которое повышает производительность.",
                        "key": "p",
                    },
                    {
                        "value": "test",
                        "name": "🧪 тесты: Добавление недостающих тестов.",
                        "key": "t",
                    },
                    {
                        "value": "build",
                        "name": "🏗️ сборка: Изменения, которые влияют на систему сборки или внешние зависимости (примеры областей: pip, docker, npm).",
                        "key": "b",
                    },
                    {
                        "value": "ci",
                        "name": "🔄 непрерывная интеграция: Изменения в ваших файлах конфигурации CI и скриптах (примеры областей: GitLabCI).",
                        "key": "c",
                    },
                ],
            },
            {
                "type": "input",
                "name": "scope",
                "message": "🎯 Какая область этого изменения? (название класса или файла): (нажмите enter, чтобы пропустить).\n"
                ,
                "filter": parse_scope,
            },
            {
                "type": "input",
                "name": "subject",
                "filter": parse_subject,
                "message": "📝 Напишите краткое и повелительное описание изменений в коде: (в нижнем регистре и без точки)\n"
                ,
            },
            {
                "type": "input",
                "name": "body",
                "message": "📝 Укажите дополнительную контекстную информацию об изменениях в коде: (нажмите [enter], чтобы пропустить).\n"
                ,
                "filter": multiple_line_breaker,
            },
            {
                "type": "confirm",
                "name": "is_breaking_change",
                "message": "⚠️ Это является СЕРЬЕЗНЫМ ИЗМЕНЕНИЕМ?",
                "default": False,
            },
            {
                "type": "input",
                "name": "footer",
                "message": "📝 Перечислите любые ЗАКРЫТЫЕ ПРОБЛЕМЫ, связанные с этим изменением: (нажмите [enter], чтобы пропустить). Например: #31, #34\n"
                ,
            },
        ]
        return questions

    def message(self, answers: dict) -> str:
        prefix = answers["prefix"]
        scope = answers["scope"]
        subject = answers["subject"]
        body = answers["body"]
        footer = answers["footer"]
        is_breaking_change = answers["is_breaking_change"]

        if scope:
            scope = f"({scope})"
        if body:
            body = f"\n\n{body}"
        if is_breaking_change:
            footer = f"BREAKING CHANGE: {footer}"
        if footer:
            footer = f"\n\n{footer}"

        message = f"{prefix}{scope}: {subject}{body}{footer}"

        return message

    def example(self) -> str:
        return (
            "fix: correct minor typos in code\n"
            "\n"
            "see the issue for details on the typos fixed\n"
            "\n"
            "closes issue #12"
        )

    def schema(self) -> str:
        return (
            "<type>(<scope>): <subject>\n"
            "<BLANK LINE>\n"
            "<body>\n"
            "<BLANK LINE>\n"
            "(BREAKING CHANGE: )<footer>"
        )

    def schema_pattern(self) -> str:
        PATTERN = (
            r"(?s)"  # To explicitly make . match new line
            r"(build|ci|docs|feat|fix|perf|refactor|style|test|chore|revert|bump)"  # type
            r"(\(\S+\))?!?:"  # scope
            r"( [^\n\r]+)"  # subject
            r"((\n\n.*)|(\s*))?$"
        )
        return PATTERN

    def info(self) -> str:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        filepath = os.path.join(dir_path, "conventional_commits_info.txt")
        with open(filepath, encoding=self.config.settings["encoding"]) as f:
            content = f.read()
        return content

    def process_commit(self, commit: str) -> str:
        pat = re.compile(self.schema_pattern())
        m = re.match(pat, commit)
        if m is None:
            return ""
        return m.group(3).strip()
